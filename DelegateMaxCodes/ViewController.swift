//
//  ViewController.swift
//  DelegateMaxCodes
//
//  Created by MacBook Pro on 11/17/19.
//  Copyright © 2019 Black Beard Games. All rights reserved.
//

import UIKit

protocol DataDelegate {//create the protocol
    func printThisString(string: String)//create a function to be delegated

}

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
        // associate this view controller to the delegate
        
        perform(#selector(advance), with: nil, afterDelay: 2)
        
    }

    @objc func advance() {
        let vc = ViewController2()
        vc.delegate = self
        present(vc, animated: true, completion: nil)
        //the code inside this func is associating the view controller 
    }

}

extension ViewController: DataDelegate {
    func printThisString(string: String) {
        print(string)
    }
    
}
