//
//  ViewController2.swift
//  DelegateMaxCodes
//
//  Created by MacBook Pro on 11/17/19.
//  Copyright © 2019 Black Beard Games. All rights reserved.
//

import UIKit

class ViewController2: UIViewController {
    
    var delegate: DataDelegate?//this is the boss class is who is delegating

    override func viewDidLoad() {
        super.viewDidLoad()
        
        delegate?.printThisString(string: "Move this to first view controller")
        //up above is just the delegate variable and the function of protocol
        
    }

}
